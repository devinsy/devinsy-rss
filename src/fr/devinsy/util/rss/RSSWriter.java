/*
 * Copyright (C) 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-rss.
 * 
 * Devinsy-rss is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-rss is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-rss.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.rss;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import fr.devinsy.util.xml.XMLWriter;

/**
 * The Class RSSWriter.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class RSSWriter
{
	private XMLWriter out;

	/**
	 * Instantiates a new RSS writer.
	 * 
	 * @param file
	 *            the file
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public RSSWriter(final File file) throws UnsupportedEncodingException, FileNotFoundException
	{
		this.out = new XMLWriter(file);
		writeRSSHeader();
	}

	/**
	 * Instantiates a new RSS writer.
	 * 
	 * @param target
	 *            the target
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 */
	public RSSWriter(final OutputStream target) throws UnsupportedEncodingException
	{
		this.out = new XMLWriter(target);
		writeRSSHeader();
	}

	/**
	 * Instantiates a new RSS writer.
	 * 
	 * @param target
	 *            the target
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 */
	public RSSWriter(final Writer target) throws UnsupportedEncodingException
	{
		this.out = new XMLWriter(target);
		writeRSSHeader();
	}

	/**
	 * Close.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void close() throws IOException
	{
		if (this.out != null)
		{
			this.out.writeEndTag("channel");
			this.out.writeEndTag("rss");
			this.out.close();
		}
	}

	/**
	 * Flush.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void flush() throws IOException
	{
		if (this.out != null)
		{
			this.out.flush();
		}
	}

	/**
	 * Write channel.
	 * 
	 * @param title
	 *            the title
	 * @param link
	 *            the link
	 * @param description
	 *            the description
	 * @param elements
	 *            the elements
	 */
	public void writeChannel(final String title, final String link, final String description, final RSSElement... elements)
	{
		//
		this.out.writeStartTag("channel");

		//
		this.out.writeTag("title", title);
		this.out.writeTag("link", link);
		this.out.writeTag("description", description);

		//
		if ((elements != null) && (elements.length > 0))
		{
			for (RSSElement element : elements)
			{
				this.out.writeTag(element.getName(), element.getValue(), element.getAttributes());
			}
		}
	}

	/**
	 * Write comment.
	 * 
	 * @param comment
	 *            the comment
	 */
	public void writeComment(final String comment)
	{
		this.out.writeComment(comment);
	}

	/**
	 * Write item.
	 * 
	 * @param title
	 *            the title
	 * @param link
	 *            the link
	 * @param description
	 *            the description
	 * @param elements
	 *            the elements
	 */
	public void writeItem(final String title, final String link, final String description, final RSSElement... elements)
	{
		//
		this.out.writeStartTag("item");

		//
		this.out.writeTag("title", title);
		if (link != null)
		{
			this.out.writeTag("link", link);
		}
		if (description == null)
		{
			this.out.writeTag("description", "n/a");
		}
		else
		{
			this.out.writeTag("description", description);
		}

		//
		if ((elements != null) && (elements.length > 0))
		{
			for (RSSElement element : elements)
			{
				this.out.writeTag(element.getName(), element.getValue(), element.getAttributes());
			}
		}

		//
		this.out.writeEndTag("item");
	}

	/**
	 * Write RSS header.
	 */
	private void writeRSSHeader()
	{
		this.out.writeXMLHeader();
		this.out.writeStartTag("rss", "version", "2.0");
	}
}
