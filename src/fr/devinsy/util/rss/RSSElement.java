/*
 * Copyright (C) 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-rss.
 * 
 * Devinsy-rss is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-rss is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-rss.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.rss;

import org.joda.time.DateTime;

/**
 * The Class RSSElement.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class RSSElement
{
	public static final String DATE_PATTERN = "dd MMM YYYY hh:mm:ss Z";

	private String name;
	private String value;
	private String[] attributes;

	/**
	 * Instantiates a new RSS element.
	 * 
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 */
	public RSSElement(final String name, final DateTime value)
	{
		setName(name);
		if (value == null)
		{
			this.value = null;
		}
		else
		{
			this.value = value.toString(DATE_PATTERN);
		}
		this.attributes = null;
	}

	/**
	 * Instantiates a new RSS element.
	 * 
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 */
	public RSSElement(final String name, final long value)
	{
		setName(name);
		this.value = String.valueOf(value);
		this.attributes = null;
	}

	/**
	 * Instantiates a new RSS element.
	 * 
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 * @param attributes
	 *            the attributes
	 */
	public RSSElement(final String name, final long value, final String... attributes)
	{
		setName(name);
		this.value = String.valueOf(value);
		this.attributes = attributes;
	}

	/**
	 * Instantiates a new RSS element.
	 * 
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 */
	public RSSElement(final String name, final String value)
	{
		setName(name);
		this.value = value;
		this.attributes = null;
	}

	/**
	 * Instantiates a new RSS element.
	 * 
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 * @param attributes
	 *            the attributes
	 */
	public RSSElement(final String name, final String value, final String... attributes)
	{
		setName(name);
		this.value = value;
		this.attributes = attributes;
	}

	/**
	 * Gets the attributes.
	 * 
	 * @return the attributes
	 */
	public String[] getAttributes()
	{
		return this.attributes;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue()
	{
		return this.value;
	}

	/**
	 * Sets the attributes.
	 * 
	 * @param attributes
	 *            the new attributes
	 */
	public void setAttributes(final String[] attributes)
	{
		this.attributes = attributes;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(final String name)
	{
		if (name == null)
		{
			throw new IllegalArgumentException("name is null");
		}
		else
		{
			this.name = name;
		}
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the new value
	 */
	public void setValue(final String value)
	{
		this.value = value;
	}

}
