/*
 * Copyright (C) 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-rss.
 * 
 * Devinsy-rss is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-rss is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-rss.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.rss;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import fr.devinsy.util.strings.StringList;

/**
 * The Class RSSCache.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class RSSCache
{
	private static RSSCache instance = new RSSCache();
	private Map<String, String> cache;

	/**
	 * Instantiates a new RSS cache.
	 */
	private RSSCache()
	{
		this.cache = new HashMap<String, String>();
	}

	/**
	 * Gets the.
	 * 
	 * @param name
	 *            the name
	 * @return the string
	 */
	public String get(final String name)
	{
		String result;

		result = get(name, Locale.ROOT);

		//
		return result;
	}

	/**
	 * Gets the.
	 * 
	 * @param name
	 *            the name
	 * @param locale
	 *            the locale
	 * @return the string
	 */
	public String get(final String name, final Locale locale)
	{
		String result;

		if (name == null)
		{
			throw new IllegalArgumentException("name is null.");
		}
		else if (locale == null)
		{
			result = get(name, Locale.ROOT);
		}
		else
		{
			result = this.cache.get(key(name, locale));

			if (result == null)
			{
				result = this.cache.get(name);
			}
		}

		//
		return result;
	}

	/**
	 * Key.
	 * 
	 * @param name
	 *            the name
	 * @param locale
	 *            the locale
	 * @return the string
	 */
	private String key(final String name, final Locale locale)
	{
		String result;

		if (locale.getLanguage().length() == 0)
		{
			result = name;
		}
		else
		{
			result = name + "_" + locale.getLanguage();
		}

		//
		return result;
	}

	/**
	 * Put.
	 * 
	 * @param name
	 *            the name
	 * @param locale
	 *            the locale
	 * @param rss
	 *            the rss
	 */
	public void put(final String name, final Locale locale, final String rss)
	{
		if (name == null)
		{
			throw new IllegalArgumentException("Name is null.");
		}
		else if (locale == null)
		{
			put(name, Locale.ROOT, rss);
		}
		else
		{
			this.cache.put(key(name, locale), rss);
		}
	}

	/**
	 * Put.
	 * 
	 * @param name
	 *            the name
	 * @param rss
	 *            the rss
	 */
	public void put(final String name, final String rss)
	{
		put(name, Locale.ROOT, rss);
	}

	/**
	 * Removes the.
	 * 
	 * @param name
	 *            the name
	 */
	public void remove(final String name)
	{
		remove(name, Locale.ROOT);
	}

	/**
	 * Removes the.
	 * 
	 * @param name
	 *            the name
	 * @param locale
	 *            the locale
	 */
	public void remove(final String name, final Locale locale)
	{
		if (name == null)
		{
			throw new IllegalArgumentException("Name is null.");
		}
		else
		{
			this.cache.remove(name + "-" + locale.getLanguage());
		}
	}

	/**
	 * Sets the oudated.
	 * 
	 * @param name
	 *            the new oudated
	 */
	public void setOudated(final String name)
	{
		for (String subkey : subkeys(name))
		{
			if (subkey.startsWith(name))
			{
				this.cache.remove(subkey);
			}
		}
	}

	/**
	 * Subkeys.
	 * 
	 * @param name
	 *            the name
	 * @return the string list
	 */
	public StringList subkeys(final String name)
	{
		StringList result;

		//
		result = new StringList();

		//
		for (String key : this.cache.keySet())
		{
			if (key.startsWith(name))
			{
				result.add(key);
			}
		}

		//
		return result;
	}

	/**
	 * Instance.
	 * 
	 * @return the RSS cache
	 */
	public static RSSCache instance()
	{
		return instance;
	}
}
