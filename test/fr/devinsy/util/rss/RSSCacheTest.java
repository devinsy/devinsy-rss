/*
 * Copyright (C) 2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-rss.
 * 
 * Devinsy-rss is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-rss is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-rss.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.rss;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

/**
 * The Class RSSCacheTest.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class RSSCacheTest
{
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RSSCacheTest.class);

	/**
	 * Before.
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.ERROR);
	}

	/**
	 * Test 01.
	 */
	@Test
	public void test01()
	{
		//
		logger.debug("===== test starting...");

		RSSCache cache = RSSCache.instance();

		cache.put("ALPHA", "Mignonne, allons voir si la rose");
		cache.put("BRAVO", "Qui ce matin avoit desclose");
		cache.put("CHARLIE", "Sa robe de pourpre au Soleil,");

		cache.setOudated("CHARLIE");

		//
		logger.debug("===== test done.");
	}
}
